# Installation

In prod, use this command to install the project:

```sh
make install-prod
```

In dev, use:

```sh
make update-dev
```

## Admin access

You access the admin on `/console/` and the login on `/console/login`.

But first create an admin user using this command:

```sh
bin/console app:create-admin your@email.com yourpassword
```
