# Installation

En environnement de production, veuillez utiliser la commande suivante pour installer le projet:

```sh
make install-prod
```

En environnement de développement, utilisez :

```sh
make update-dev
```

## Accès à l'administration

Vous pouvez accéder à la console administrative via `/console/` ainsi qu'à l'écran de connexion sur `/console/login`.

Mais préalablement, créer un administrateur via la commande :

```sh
bin/console app:create-admin votre@adresse-electronique.com votremotdepasse
```
