# The Lyssal blog

[en]

Welcome to the Lyssal blog.

You can easily create your own Symfony blog forking this project.
See the result on [lyssal.net](https://lyssal.net).

We especially use these Lyssal bundles and librairies:

* [BlogBundle](https://github.com/Lyssal/blog-bundle) : To manage blogs, categories and posts and generate ATOM and RSS feeds ;
* [SeoBundle](https://github.com/Lyssal/seo-bundle) : To manage page contents, define meta tags for SEO and generate the `sitemap.xml` ;
* [DoctrineOrmBundle](https://github.com/Lyssal/doctrine-orm-bundle) : To simplify Doctrine queries ;
* [EntityBundle](https://github.com/Lyssal/entity-bundle) : To facilitate the generation of entity paths or to generate breadcrumbs ;
* [SearchEngineBundle](https://github.com/Lyssal/search-engine-bundle) : To easily add a simple search field ;
* [PHP library](https://github.com/Lyssal/lib-php) : Some useful classes to manage images and files, generate slugs, etc ;
* [Ajax page loader](https://github.com/Lyssal/ajax-page-loader) : To easily use AJAX to load pages.

For the administration, we use [EasyAdmin](https://github.com/EasyCorp/EasyAdminBundle).

## Installation

See the [installation documentation](doc/en/Installation.md)


---


[fr]

Bienvenue sur le blogue Lyssal.

Vous pouvez simplement créer votre propre blogue Symfony en créant une divergence de ce projet.
Vous pouvez visualiser le résultat sur [lyssal.net](https://lyssal.net).

Nous utilisons notamment les modules et librairies Lyssal suivantes :

* [BlogBundle](https://github.com/Lyssal/blog-bundle) : Pour gérer des blogues, catégories et articles et générer les flux ATOM et RSS ;
* [SeoBundle](https://github.com/Lyssal/seo-bundle) : Pour gérer les contenus des pages, définir les métadonnées pour le référencement naturel et générer le `sitemap.xml` ;
* [DoctrineOrmBundle](https://github.com/Lyssal/doctrine-orm-bundle) : Pour simplifier les requêtes Doctrine ;
* [EntityBundle](https://github.com/Lyssal/entity-bundle) : Pour faciliter la génération d'URL pour les entités ou générer le fil d'Ariane ;
* [SearchEngineBundle](https://github.com/Lyssal/search-engine-bundle) : Pour facilement ajouter un simple champ de recherche ;
* [PHP library](https://github.com/Lyssal/lib-php) : Quelques classes utilitaires pour faciliter la gestion d'images et fichers, générer des slugs, etc ;
* [Ajax page loader](https://github.com/Lyssal/ajax-page-loader) : Pour utiliser simplement l'AJAX pour charger les pages.

Pour la console administrative, nous utilisons [EasyAdmin](https://github.com/EasyCorp/EasyAdminBundle).

## Installation

Veuillez lire la [documentation d'installation](doc/fr/Installation.md)
