serve:
	bin/console server:run --env=dev

install-prod:
	composer install
	composer dump-autoload --optimize
	bin/console ckeditor:install --clear=skip
	yarn install
	make assets-prod
	make cache-clear-prod

update-dev:
	composer update
	bin/console ckeditor:install
	yarn install
	make assets-dev
	make cache-clear-dev

cache-clear-dev:
	bin/console cache:clear --no-warmup --env=dev
	bin/console cache:warmup --env=dev

cache-clear-prod:
	bin/console cache:clear --no-warmup --env=prod
	bin/console cache:warmup --env=prod

cache-clear:
	make cache-clear-prod
	make cache-clear-dev

assets-dev:
	bin/console assets:install --symlink
	yarn encore dev

assets-watch:
	bin/console assets:install --symlink
	yarn encore dev --watch

assets-prod:
	bin/console assets:install --env=prod
	yarn encore production
