/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
import '../css/app.scss';
import '@mdi/font/scss/materialdesignicons.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

require('../../assets/js/_modules/foundation.js');
require('../../assets/js/_modules/highlighter.js');
require('../../assets/js/_modules/lyssal.js');
