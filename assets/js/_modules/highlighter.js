import highlight from 'highlight.js/lib/highlight';

require('highlight.js/styles/ir-black.css');

import apache from 'highlight.js/lib/languages/apache';
highlight.registerLanguage('apache', apache);
import bash from 'highlight.js/lib/languages/bash';
highlight.registerLanguage('bash', bash);
import css from 'highlight.js/lib/languages/css';
highlight.registerLanguage('css', css);
import javascript from 'highlight.js/lib/languages/javascript';
highlight.registerLanguage('javascript', javascript);
import json from 'highlight.js/lib/languages/json';
highlight.registerLanguage('json', json);
import php from 'highlight.js/lib/languages/php';
highlight.registerLanguage('php', php);
import scss from 'highlight.js/lib/languages/scss';
highlight.registerLanguage('scss', scss);
import twig from 'highlight.js/lib/languages/twig';
highlight.registerLanguage('twig', twig);
import xml from 'highlight.js/lib/languages/xml';
highlight.registerLanguage('xml', xml);
import yaml from 'highlight.js/lib/languages/yaml';
highlight.registerLanguage('yaml', yaml);

highlight.initHighlightingOnLoad();
