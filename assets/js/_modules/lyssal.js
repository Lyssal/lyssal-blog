import '@lyssal/blinking/lib/blinking.css';
import AjaxPageLoader from '@lyssal/ajax-page-loader/lib/ajax-page-loader.amd';
import highlight from 'highlight.js/lib/highlight';

let ajaxPageLoader = new AjaxPageLoader();
// The element in which the page loader is used
ajaxPageLoader.setDefaultTarget('#page');

ajaxPageLoader.setBeforeContentSettingEvent((ajaxLink) => {
  // Go to the top of the page
  global.window.scrollTo(0, 0);
});

ajaxPageLoader.setAfterContentSettingEvent((ajaxLink) => {
  // Update something in the DOM
  highlight.initHighlighting.called = false;
  highlight.initHighlighting();

  // Update the tab title
  document.title = document.querySelector('h1').textContent;
});

ajaxPageLoader.setAfterAjaxLoadingEvent((ajaxLink) => {
  // Change the browser tab title
  const url = ajaxLink.getUrl();
  global.history.pushState({}, null, url);
});

// As we dynamically change the URL, we refresh the page when the user use back / forward buttons
window.addEventListener('popstate', function(event) {
  window.location.href = window.location.pathname;
}, false);
