<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\EventListener;

use App\Exception\NotFoundException;
use Lyssal\BlogBundle\Manager\BlogManager;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Event which inject the blog into the template.
 */
class TemplateBlogVariable
{
    /**
     * @var \Twig_Environment The template engine
     */
    private $templating;

    /**
     * @var \Lyssal\BlogBundle\Manager\BlogManager The blog manager
     */
    private $blogManager;

    /**
     * @var int The blog ID
     */
    private $blogId;


    /**
     * TemplateBlogVariable constructor.
     *
     * @param \Twig_Environment                      $templating  The template engine
     * @param \Lyssal\BlogBundle\Manager\BlogManager $blogManager The blog manager
     * @param int                                    $blogId      The blog ID
     */
    public function __construct( $templating, BlogManager $blogManager, int $blogId)
    {
        $this->templating = $templating;
        $this->blogManager = $blogManager;
        $this->blogId = $blogId;
    }


    /**
     * Inject the blog into templating if It does not exist.
     *
     * @param \Symfony\Component\HttpKernel\Event\FilterControllerEvent $event The event
     * @throws \App\Exception\NotFoundException If the blog is not found
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $blog = $this->blogManager->findOneById($this->blogId);
        if (null === $blog) {
            throw new NotFoundException('The blog ID='.$this->blogId.' has not been found. Do you have specified the good Lyssal blog ID in your environnement file?');
        }

        $this->templating->addGlobal('blog', $blog);
    }
}
