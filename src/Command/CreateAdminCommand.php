<?php
namespace App\Command;

use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Command to create an admin.
 */
class CreateAdminCommand extends Command
{
    private $passwordEncoder;

    private $userManager;


    public function __construct(UserPasswordEncoderInterface $passwordEncoder, UserManager $userManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userManager = $userManager;

        parent::__construct('app:create-admin');
    }


    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'The admin email')
            ->addArgument('password', InputArgument::REQUIRED, 'The admin password')
            ->setDescription('Create an admin user')
        ;
    }

    /**
     * @inheritDoc
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');

        $user = new User();
        $user->setEmail($email);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
        $user->setRoles(['ROLE_ADMIN']);

        $this->userManager->save($user);
    }
}
