<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Entity\Seo;

use Doctrine\ORM\Mapping as ORM;
use Lyssal\SeoBundle\Entity\Host as LyssalHost;

/**
 * @inheritDoc
 *
 * @ORM\Entity()
 */
class Host extends LyssalHost
{

}
