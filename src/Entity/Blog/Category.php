<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Entity\Blog;

use Doctrine\ORM\Mapping as ORM;
use Lyssal\BlogBundle\Entity\Category as LyssalCategory;

/**
 * @inheritDoc
 *
 * @ORM\Entity()
 * @ORM\Table(indexes={@ORM\Index(columns={"position"})})
 */
class Category extends LyssalCategory
{
    /**
     * {@inheritDoc}
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Seo\Page", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $page;
}
