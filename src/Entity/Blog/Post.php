<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Entity\Blog;

use App\Entity\Blog\Traits\ImageTrait;
use Doctrine\ORM\Mapping as ORM;
use Lyssal\BlogBundle\Entity\Post as LyssalPost;

/**
 * @inheritDoc
 *
 * @ORM\Entity()
 */
class Post extends LyssalPost
{
    use ImageTrait;


    /**
     * @var int The image max width
     */
    const IMAGE_MAX_WIDTH = 800;

    /**
     * @var int The image max height
     */
    const IMAGE_MAX_HEIGHT = 600;


    /**
     * {@inheritDoc}
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Seo\Page", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $page;


    /**
     * {@inheritDoc}
     * Fix for CKEditor.
     */
    public function setBody(?string $body): LyssalPost
    {
        return parent::setBody(preg_replace('/<pre>(\s*)<code([^\>]*)>(\s*)/', '<pre><code$2>', $body));
    }
}
