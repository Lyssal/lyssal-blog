<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Entity\Blog\Traits;

use App\Entity\Blog\Post;
use Doctrine\ORM\Mapping as ORM;
use Lyssal\EntityBundle\Traits\UploadedFileTrait;
use Lyssal\File\File;
use Lyssal\File\Image;

/**
 * Add a image property.
 */
trait ImageTrait
{
    use UploadedFileTrait;


    /**
     * @var string The image path
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageFilename;


    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     */
    private $uploadedFile;


    /**
     * Get the image filename.
     *
     * @return null|string The filename
     */
    public function getImageFilename(): ?string
    {
        return $this->imageFilename;
    }

    /**
     * Set the image filename.
     *
     * @param null|string $imageFilename The filename
     *
     * @return self
     */
    public function setImageFilename(?string $imageFilename): self
    {
        $this->imageFilename = $imageFilename;

        return $this;
    }


    /**
     * Return if there is an image.
     *
     * @return bool If there is an image
     */
    public function hasImage()
    {
        return null !== $this->imageFilename;
    }

    /**
     * Get the image path.
     *
     * @return string The image path
     */
    public function getImagePath()
    {
        return $this->getUploadedFileDirectory().'/'.$this->imageFilename;
    }

    /**
     * @see \Lyssal\Entity\Traits\UploadedFileTrait::getUploadedFileDirectory()
     */
    public function getUploadedFileDirectory()
    {
        return 'images'.DIRECTORY_SEPARATOR.'blog';
    }

    /**
     * Save the uploaded image.
     *
     * @return void
     */
    public function uploadFile()
    {
        // Delete the old file if existing
        $this->deleteFile();
        // Save the file in the server
        $this->saveUploadedFile();

        // Here our image in the server
        $image = new Image($this->getUploadedFilePathname());
        // We minify the image name to remove special characters,
        // specify a maxlength for the database
        // and to not replace an existing file
        $image->minify(null, null, true, 255);
        // We get the new filename
        $this->imageFilename = $image->getFilename();

        // We verify that the image format is managed
        if ($image->formatIsManaged()) {
            // We proportionally reduce the image
            $image->resizeProportionallyByMaxSize(Post::IMAGE_MAX_WIDTH, Post::IMAGE_MAX_HEIGHT);
        }
    }

    /**
     * Delete the image.
     */
    protected function deleteFile()
    {
        if (null !== $this->imageFilename) {
            $file = new File($this->getImagePath());
            $file->delete();
        }
    }
}
