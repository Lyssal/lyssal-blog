<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Appellation;

use Lyssal\BlogBundle\Appellation\PostAppellation as LyssalBlogPostAppellation;

/**
 * @inheritDoc
 */
class PostAppellation extends LyssalBlogPostAppellation
{
    /**
     * @inheritDoc
     */
    public function appellationHtml($object)
    {
        $url =  $this->entityRouterManager->generate($object);

        return '<a href="'.$url.'" data-ajax="true">'.$this->appellation($object).'</a>';
    }
}
