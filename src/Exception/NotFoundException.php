<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Exception;

use Exception;

/**
 * Exception when a resource / entity is not found.
 */
class NotFoundException extends Exception
{

}
