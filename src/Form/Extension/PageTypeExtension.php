<?php
/**
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 * @author Rémi Leclerc
 */
namespace App\Form\Extension;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Lyssal\SeoBundle\Form\Type\PageType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Extends the Page form.
 */
class PageTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $contentOptions = $builder->get('content')->getOptions();

        $builder
            ->remove('content')
            ->add('content', CKEditorType::class, $contentOptions)
        ;
    }


    /**
     * @see \Symfony\Component\Form\FormTypeExtensionInterface::getExtendedType()
     */
    public function getExtendedType()
    {
        return PageType::class;
    }
}
